CREATE TABLE [dbo].[Appointments]
(
[EmployeeID] [int] NOT NULL,
[StartTime] [datetime] NOT NULL,
[EndTime] [datetime] NOT NULL,
[ClientID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Appointments] ADD CONSTRAINT [PK_Appointments] PRIMARY KEY CLUSTERED  ([EmployeeID], [StartTime]) ON [PRIMARY]
GO
