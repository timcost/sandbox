CREATE TABLE [dbo].[Server]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ServerName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DayOnline] [date] NOT NULL
) ON [PRIMARY]
GO
