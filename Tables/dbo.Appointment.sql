CREATE TABLE [dbo].[Appointment]
(
[DoctorID] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Date] [date] NOT NULL,
[StartTime] [time] (0) NOT NULL,
[EndTime] [time] (0) NOT NULL,
[Status] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserID] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Price] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Appointment] ADD CONSTRAINT [CHK_EndTime_After_StartTime] CHECK (([EndTime]>[StartTime]))
GO
ALTER TABLE [dbo].[Appointment] ADD CONSTRAINT [CHK_EndTime_BusinessHours] CHECK ((datepart(hour,[EndTime])>=(7) AND datepart(hour,dateadd(second,(-1),[EndTime]))<=(16)))
GO
ALTER TABLE [dbo].[Appointment] ADD CONSTRAINT [CHK_StartTime_BusinessHours] CHECK ((datepart(hour,[StartTime])>=(7) AND datepart(hour,[StartTime])<=(16)))
GO
ALTER TABLE [dbo].[Appointment] ADD CONSTRAINT [PK_Appointment] PRIMARY KEY CLUSTERED  ([DoctorID], [Date], [StartTime]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [iDoctor_End] ON [dbo].[Appointment] ([DoctorID], [Date], [EndTime]) ON [PRIMARY]
GO
